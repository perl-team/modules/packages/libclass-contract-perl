Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Class-Contract
Upstream-Contact: C. Garrett Goebel (ggoebel@cpan.org)
Upstream-Name: Class-Contract

Files: *
Copyright: 1997-2000, Damian Conway (damian@conway.org)
 2000-2001, C. Garrett Goebel (ggoebel@cpan.org)
License: Artistic

Files: debian/*
Copyright: 2003, Paul Baker (w2gi) <pbaker@where2getit.com>
 2006, Luk Claes <luk@debian.org>
 2007-2013, Nacho Barrientos Arias <nacho@debian.org>
 2014-2023, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
